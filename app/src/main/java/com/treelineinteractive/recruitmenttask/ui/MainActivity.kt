package com.treelineinteractive.recruitmenttask.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import com.treelineinteractive.recruitmenttask.R
import com.treelineinteractive.recruitmenttask.databinding.ActivityMainBinding
import com.treelineinteractive.recruitmenttask.ui.view.ProductItemView


class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)
    private val mainViewModel = MainViewModel()

    private val incrementAction: (Any, ProductItemView) -> Unit = { tag, view ->
        if (tag is String) {
            val totalSold = mainViewModel.incrementSale(tag)
            view.findViewById<TextView>(R.id.itemSoldLabel).text =
                String.format(getString(R.string.total_sold), totalSold)
        }
    }

    private val decrementAction: (Any, ProductItemView) -> Unit = { tag, view ->
        if (tag is String) {
            val totalSold = mainViewModel.decrementSale(tag)
            view.findViewById<TextView>(R.id.itemSoldLabel).text =
                String.format(getString(R.string.total_sold), totalSold)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        mainViewModel.loadProducts()

        binding.retryButton.setOnClickListener {
            mainViewModel.loadProducts()
        }

        mainViewModel.stateLiveData.observe { state ->
            binding.progressBar.isVisible = state.isLoading
            binding.errorLayout.isVisible = state.error != null
            binding.errorLabel.text = state.error

            binding.itemsLayout.isVisible = state.isSuccess
            binding.submitButton.isVisible = state.isSuccess

            binding.submitButton.setOnClickListener {
                submitSalesData(mainViewModel.getSalesData())
            }

            if (state.isSuccess) {
                binding.itemsLayout.removeAllViews()

                state.items.forEach {
                    val itemView = ProductItemView(this)
                    itemView.setProductItem(it)
                    val params = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )

                    params.topMargin = 16
                    params.marginEnd = 16
                    params.marginStart = 16

                    itemView.layoutParams = params

                    binding.itemsLayout.addView(itemView, binding.itemsLayout.childCount)

                    itemView.binding.incrementButton.setOnClickListener { _ ->
                        incrementAction(it.id, itemView)
                    }

                    itemView.binding.decrementButton.setOnClickListener { _ ->
                        decrementAction(it.id, itemView)
                    }
                }
            }
        }
    }

    private fun submitSalesData(salesData: String) {
        val intent = getEmailIntent(salesData, SALES_RECIPIENT)
        startActivity(intent)
    }

    fun getEmailIntent(salesData: String, recipient: String) = Intent(Intent.ACTION_SENDTO).apply {
        putExtra(Intent.EXTRA_SUBJECT, SALES_REPORT_SUBJECT)
        putExtra(Intent.EXTRA_TEXT, "$SALES_REPORT_BODY$salesData")
        data = Uri.parse(recipient)
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    }

    fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) {
        observer(this@MainActivity, onChanged)
    }

    companion object {
        const val SALES_REPORT_SUBJECT = "Sales Report"
        const val SALES_REPORT_BODY = "Sales Data: "
        const val SALES_RECIPIENT = "mailto:bossman@bosscompany.com"
    }

}