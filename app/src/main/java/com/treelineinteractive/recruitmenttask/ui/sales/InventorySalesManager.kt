package com.treelineinteractive.recruitmenttask.ui.sales

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem

class InventorySalesManager {

    private val salesData: MutableMap<String, Int> = HashMap()

    fun incrementSales(productItem: ProductItem): Int {
        var currentSales: Int = salesData[productItem.id] ?: 0
        val upperBound = productItem.available

        if (currentSales < upperBound) {
            currentSales++
        }

        salesData[productItem.id] = currentSales
        return currentSales
    }

    fun decrementSales(productItem: ProductItem): Int {
        var currentSales: Int = salesData[productItem.id] ?: 0
        val lowerBound = 0

        if (currentSales > lowerBound) {
            currentSales--
        }

        salesData[productItem.id] = currentSales
        return currentSales
    }

    fun getSalesData() = salesData.toMutableMap()
}