package com.treelineinteractive.recruitmenttask.ui.sales

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito

class InventorySalesManagerTest {

    val sut = InventorySalesManager()

    @Test
    fun `incrementSales no current sales, updates to 1`() {

        // GIVEN
        val productItem = Mockito.mock(ProductItem::class.java)
        Mockito.`when`(productItem.id).thenReturn("1")
        Mockito.`when`(productItem.available).thenReturn(1)

        // WHEN
        sut.incrementSales(productItem)

        // THEN
        val result = sut.getSalesData()
        assertEquals(result[productItem.id], 1)
    }

    @Test
    fun `incrementSales sales at max, does not increment`() {

        // GIVEN
        val productItem = Mockito.mock(ProductItem::class.java)
        Mockito.`when`(productItem.id).thenReturn("1")
        Mockito.`when`(productItem.available).thenReturn(1)

        // WHEN
        sut.incrementSales(productItem)
        sut.incrementSales(productItem)

        // THEN
        val result = sut.getSalesData()
        assertEquals(result[productItem.id], 1)
    }

    @Test
    fun `decrementSales no current sales, does not update`() {

        // GIVEN
        val productItem = Mockito.mock(ProductItem::class.java)
        Mockito.`when`(productItem.id).thenReturn("1")

        // WHEN
        sut.decrementSales(productItem)

        // THEN
        val result = sut.getSalesData()
        assertEquals(result[productItem.id], 0)
    }

    @Test
    fun `decrementSales has sales, decrements by 1`() {

        // GIVEN
        val productItem = Mockito.mock(ProductItem::class.java)
        Mockito.`when`(productItem.id).thenReturn("1")
        Mockito.`when`(productItem.available).thenReturn(2)

        // WHEN
        sut.incrementSales(productItem)
        sut.incrementSales(productItem) // Increment by two
        sut.decrementSales(productItem)

        // THEN
        val result = sut.getSalesData()
        assertEquals(result[productItem.id], 1)
    }
}